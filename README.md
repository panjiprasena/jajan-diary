# diary-jajan

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

Berikut Soal 2 pemberian saran perbaikan kompas.id : 

1. Tampilan kontras terlalu tajam, menjadikan kurangnya kenyamanan saat membaca site dalam maktu yang cukup lama,
2. disaran kan menggunakan futur darkmode agar lebih mudah membaca saat malam hadi atau di tempat yang kurang cahaya
3. Sekilas dengan melihat skor di googlelight house, website sudah memeiliki skor accessibility, best practice dan seo yang tinggi, namun masih memiliki skor performance yang terbilang rendah, beberapa rekomendasi yang ada di lighthouse adalah meningkatkan performa dengan mengurangi waktu eksekusi javascript dan menggunakan height dan weidth di image, selengkapnya bisa dilihat dengan ekstensi google lighthouse
4. Tampilan kurang menarik bila dibandingkan dengan website kompetitor seperti tirto.id yang lebih eye-catching dan simple5.
5. desain website yang menonjolkan bentuk seperti koran kovensional, hal ini sebenarnya cukup bagus dalam memberikan ciri khas tersendiri namun menjadikanya kurang menarik 
